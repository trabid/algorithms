package company.paternRecognition;

import java.util.LinkedList;

public class BruteCollinearPoints {
    private LinkedList<LineSegment> segments;
    private int size = 0;

    public BruteCollinearPoints(Point[] points) {
        if (points != null && correctData(points)) {
            final int length = points.length;
            segments = new LinkedList<>();
            for (int i = 0; i < length; i++) {
                for (int j = 0; j < length; j++) {
                    if (j == i) continue;
                    for (int k = 0; k < length; k++) {
                        if (k == j || k == i) continue;
                        if ((points[i].slopeTo(points[j])) != (points[i].slopeTo(points[k]))) continue;
                        for (int l = 0; l < length; l++) {
                            if (l == j || l == i || l == k) continue;
                            if ((points[i].slopeTo(points[j])) != (points[i].slopeTo(points[k]))) continue;
                            if (points[i].slopeTo(points[j]) != (points[i].slopeTo(points[l]))) continue;
                            if (points[i].compareTo(points[j]) >= 0 ||
                                    (points[j].compareTo(points[k]) >= 0) ||
                                    (points[k].compareTo(points[l]) >= 0)) {
                                continue;
                            }
                            segments.add(new LineSegment(points[i], points[l]));
                            size++;
                        }
                    }
                }
            }
        } else {
            throw new IllegalArgumentException("Argumet is incorrect");
        }
    }

    public int numberOfSegments() {
        return size;
    }

    public LineSegment[] segments() {
        LineSegment[] a = new LineSegment[size];
        segments.toArray(a);
        return a;
    }

    private boolean correctData(Point[] points) {
        if (points.length == 1 && points[0] == null) {
            return false;
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                if (i == j) continue;
                if (points[i] == null || points[j] == null || points[i].compareTo(points[j]) == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}
