package company.paternRecognition;

import java.util.Arrays;
import java.util.LinkedList;

public class FastCollinearPoints {
    private LinkedList<LineSegment> segments;
    private int size = 0;

    public FastCollinearPoints(Point[] points) {
        if (points != null && points.length > 0 && correctData(points)) {
            int length = points.length;
            segments = new LinkedList<>();
            Point[] copy = points.clone();
            Arrays.sort(copy);
            for (int i = 0; i < length - 3; i++) {
                Arrays.sort(copy);
                Arrays.sort(copy, copy[i].slopeOrder());
                for (int p = 0, start = 1, end = 2; end < length; end++) {
                    while (end < copy.length && Double.compare(copy[p].slopeTo(copy[start]), copy[p].slopeTo(copy[end])) == 0) {
                        end++;
                    }
                    if (end - start > 2 && copy[p].compareTo(copy[start]) < 0) {
                        segments.add(new LineSegment(copy[p], copy[end - 1]));
                        size++;
                    }
                    start = end;
                }
            }
        } else {
            throw new IllegalArgumentException("Argument is incorrect");
        }
    }

    public int numberOfSegments() {
        return size;
    }

    public LineSegment[] segments() {
        LineSegment[] a = new LineSegment[segments.size()];
        segments.toArray(a);
        return a;
    }

    private boolean correctData(Point[] points) {
        if (points.length == 1 && points[0] == null) {
            return false;
        }
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points.length; j++) {
                if (i == j) continue;
                if (points[i] == null || points[j] == null || points[i].compareTo(points[j]) == 0) {
                    return false;
                }
            }
        }
        return true;
    }
}

