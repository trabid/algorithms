//import edu.princeton.cs.algs4.StdOut;
//import edu.princeton.cs.algs4.StdRandom;
//import edu.princeton.cs.algs4.StdStats;
//
//public class PercolationStats {
//
//    private static final double CONFIDENCE_95 = 1.96;
//    private final int trials;
//    private final double[] tresholds;
//    private double mean;
//    private double stdDev;
//
//    public PercolationStats(int n, int trials) {
//        if (n <= 0 || trials <= 0) {
//            throw new IllegalArgumentException("Invalid argument, argument should be above zero");
//        }
//        this.trials = trials;
//        tresholds = getTresholds(n );
////    }
//
//    public double mean() {
//        mean = StdStats.mean(tresholds);
//        return mean;
//    }
//
//    public double stddev() {
//        stdDev = StdStats.stddev(tresholds);
//        return stdDev;
//    }
//
//    public double confidenceLo() {
//        return mean - CONFIDENCE_95 * stddev() / Math.sqrt(trials);
//    }
//
//    public double confidenceHi() {
//        return stdDev + CONFIDENCE_95 * stddev() / Math.sqrt(trials);
//    }
//
//    private double[] getTresholds(int size) {
//        Percolation percolation;
//        double[] tab = new double[trials];
//        for (int i = 0; i < trials; i++) {
//            percolation = new Percolation(size);
//            while (!percolation.percolates()) {
//
//                percolation.open(StdRandom.uniform(1, size + 1), StdRandom.uniform(1, size + 1));
//            }
//            tab[i] = (double) percolation.numberOfOpenSites() / (size * size);
//        }
//        return tab;
//    }
//
//    public static void main(String[] args) {
//        int size = Integer.parseInt(args[0]);
//        int times = Integer.parseInt(args[1]);
//        PercolationStats percolationStats = new PercolationStats(size, times);
//        StdOut.println("Mean: " + percolationStats.mean());
//        StdOut.println("Standard deviation: " + percolationStats.stddev());
//        StdOut.println("95% confidence interval: " + percolationStats.confidenceLo() + "," + percolationStats.confidenceHi());
//    }
//}
