package company.percolation;

import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.WeightedQuickUnionUF;

public class Percolation {


    private static final boolean OPEN = true;
    private static final int TOP = 0;
    private final int size;
    private final int bottom;
    private final WeightedQuickUnionUF quickUnion;
    private final WeightedQuickUnionUF backwash;
    private boolean[][] grid;
    private int numberOfOpenSites;


    public Percolation(int n) {
        if (n <= 0) {
            throw new IllegalArgumentException("Argument is below or eqauls 0");
        }
        this.size = n;
        quickUnion = new WeightedQuickUnionUF(this.size * this.size + 1);
        backwash = new WeightedQuickUnionUF(this.size * this.size + 2);
        grid = new boolean[this.size][this.size];
        bottom = this.size * this.size + 1;
        numberOfOpenSites = 0;

    }

    public void open(int row, int col) {
        checkException(row, col);

        if (!isOpen(row, col)) {
            grid[row - 1][col - 1] = OPEN;
            numberOfOpenSites++;
            if (row == 1) {
                quickUnion.union(getGridIndex(row, col), TOP);
                backwash.union(getGridIndex(row, col), TOP);
            }

            if (row == size) {
                backwash.union(getGridIndex(row, col), bottom);
            }

            if (row - 1 > 0 && isOpen(row - 1, col)) {
                if (!quickUnion.connected(getGridIndex(row - 1, col), getGridIndex(row, col))) {
                    quickUnion.union(getGridIndex(row - 1, col), getGridIndex(row, col));
                    backwash.union(getGridIndex(row - 1, col), getGridIndex(row, col));
                }
            }
            if (col - 1 > 0 && isOpen(row, col - 1)) {
                if (!quickUnion.connected(getGridIndex(row, col - 1), getGridIndex(row, col))) {
                    quickUnion.union(getGridIndex(row, col - 1), getGridIndex(row, col));
                    backwash.union(getGridIndex(row, col - 1), getGridIndex(row, col));
                }
            }
            if (col < size && isOpen(row, col + 1)) {
                if (!quickUnion.connected(getGridIndex(row, col + 1), getGridIndex(row, col))) {
                    quickUnion.union(getGridIndex(row, col + 1), getGridIndex(row, col));
                    backwash.union(getGridIndex(row, col + 1), getGridIndex(row, col));
                }
            }
            if (row < size && isOpen(row + 1, col)) {
                if (!quickUnion.connected(getGridIndex(row + 1, col), getGridIndex(row, col))) {
                    quickUnion.union(getGridIndex(row + 1, col), getGridIndex(row, col));
                    backwash.union(getGridIndex(row + 1, col), getGridIndex(row, col));
                }
            }
        }
    }

    public boolean isOpen(int row, int col) {
        checkException(row - 1, col - 1);
        return grid[row - 1][col - 1];
    }

    public boolean isFull(int row, int col) {
        if (0 < row && row <= size && 0 < col && col <= size) {
            return quickUnion.connected(TOP, getGridIndex(row, col));
        } else
            throw new IllegalArgumentException("The number you entered is not from section 1 to " + size + "," + row + "," + col);
    }

    public int numberOfOpenSites() {
        return numberOfOpenSites;
    }

    public boolean percolates() {
        return backwash.connected(TOP, bottom);
    }

    private void checkException(int row, int col) {
        if (row < 0 || row > size || col < 0 || col > size) {
            throw new IllegalArgumentException("The number you entered is not from section 1 to " + size + "," + row + "," + col);
        }
    }

    private int getGridIndex(int row, int col) {
        checkException(row, col);
        return (row - 1) * size + col;
    }

    public static void main(String[] args) {
        Percolation percolation = new Percolation(10);
        StdOut.println("####quick union");
//        percolation.quickUnion.union(0, 1);
        percolation.quickUnion.union(1, 1);
        percolation.quickUnion.union(2, 1);
        percolation.quickUnion.union(3, 1);
        percolation.quickUnion.union(4, 1);
        percolation.quickUnion.union(5, 1);
        percolation.quickUnion.union(6, 1);
        percolation.quickUnion.union(7, 1);
        percolation.quickUnion.union(8, 1);
        percolation.quickUnion.union(9, 1);
        percolation.quickUnion.union(10, 1);
        StdOut.println(percolation.quickUnion.connected(1, 10));

        StdOut.println("####Percolates");
        percolation.open(1, 5);
        percolation.open(2, 5);
        percolation.open(3, 5);
        StdOut.println("open " + percolation.isOpen(3, 1));
        percolation.open(4, 5);
        percolation.open(5, 5);
        StdOut.println("full " + percolation.isFull(5, 5));
        percolation.open(6, 1);
        percolation.open(7, 1);
        percolation.open(8, 1);
        percolation.open(9, 1);
        percolation.open(10, 1);
        StdOut.println(percolation.percolates());

        StdOut.println("####getIndex");
        StdOut.println(percolation.getGridIndex(1, 1));
        StdOut.println(percolation.getGridIndex(1, 2));
        StdOut.println(percolation.getGridIndex(10, 2));
        StdOut.println(percolation.getGridIndex(10, 10));
    }
}

