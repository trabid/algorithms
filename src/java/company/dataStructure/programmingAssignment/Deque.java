package company.dataStructure.programmingAssignment;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class Deque<Item> implements Iterable<Item> {

    private final Node head;
    private int size;

    public Deque() {
        head = new Node(null);
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void addFirst(Item item) {
        illegalArgument(item);
        if (head.next != null) {
            Node oldFirst = head.next;
            head.next = new Node(item, oldFirst);
        } else {
            head.next = new Node(item);
        }
        size++;
    }

    public void addLast(Item item) {
        illegalArgument(item);
        if (head.next != null) {
            Node temp = head.next;
            while (temp.next != null) {
                temp = temp.next;
            }
            temp.next = new Node(item);
        } else {
            head.next = new Node(item);
        }
        size++;
    }

    public Item removeFirst() {
        isEmptyException();
        Item item = head.next.item;
        head.next = head.next.next;
        size--;
        return item;
    }

    public Item removeLast() {
        isEmptyException();
        Node temp = head.next;
        Item item;
        if (size == 1) {
            item = head.next.item;
            head.next = null;
        } else {
            while (temp.next.next != null) {
                temp = temp.next;
            }
            item = temp.next.item;
            temp.next = null;
        }
        size--;
        return item;
    }

    @Override
    public Iterator<Item> iterator() {
        return new DequeIterator();
    }

    private class Node {
        private Item item;
        private Node next;

        public Node(Item item, Node next) {
            this.item = item;
            this.next = next;
        }

        public Node(Item item) {
            this.item = item;
        }

        public Item getItem() {
            return item;
        }

        public Node getNext() {
            return next;
        }

        public void setItem(Item item) {
            this.item = item;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

    private class DequeIterator implements Iterator<Item> {
        private Node current;

        public DequeIterator() {
            current = head;
        }

        @Override
        public boolean hasNext() {
            return current.next != null;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException("Element not found");
            }
            current = current.next;
            return current.getItem();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove not implemented");
        }
    }

    private void illegalArgument(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Argument can't be null");
        }
    }

    private void isEmptyException() {
        if (size == 0) {
            throw new NoSuchElementException("com.Deque is empty");
        }
    }
}