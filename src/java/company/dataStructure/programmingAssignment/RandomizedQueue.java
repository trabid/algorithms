package company.dataStructure.programmingAssignment;

import edu.princeton.cs.algs4.StdRandom;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] stack;
    private int size;

    public RandomizedQueue() {
        stack = (Item[]) new Object[1];
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    public void enqueue(Item item) {
        if (item == null) {
            throw new IllegalArgumentException("Argument is null");
        }
        stack[size++] = item;
        if (size == stack.length) {
            resize(stack.length);
        }
    }

    public Item dequeue() {
        noSuchException();
        int r = StdRandom.uniform(size);
        Item temp = stack[r];
        stack[r] = stack[size - 1];
        stack[--size] = null;
        if (size > 0 && size <= stack.length / 4) {
            resize(stack.length / 2);
        }
        return temp;
    }

    public Item sample() {
        noSuchException();
        int rand = StdRandom.uniform(size);
        if (stack[rand] == null) {
            throw new NoSuchElementException("No such element to give sample");
        }
        return stack[rand];
    }

    @Override
    public Iterator<Item> iterator() {
        return new RandomIterator();
    }

    private void resize(int capacity) {
        Item[] copy = (Item[]) new Object[capacity];
        for (int i = 0; i < size; i++) {
            copy[i] = stack[i];
        }
        stack = copy;
    }

    private class RandomIterator implements Iterator<Item> {
        int[] rand;
        int pos = size;

        public RandomIterator() {
            rand = new int[size];
            for (int i = 0; i < size; i++) {
                rand[i] = i;
            }
            StdRandom.shuffle(rand);
        }

        @Override
        public boolean hasNext() {
            return pos > 0;
        }

        @Override
        public Item next() {
            if (!hasNext()) {
                throw new NoSuchElementException("Element not found");
            }
            return stack[rand[--pos]];
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Remove is not supported");
        }
    }

    private void noSuchException() {
        if (size == 0) {
            throw new NoSuchElementException("Queue is empty");
        }
    }
}
