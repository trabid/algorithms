package company.dataStructure;

public class LinkedQueue<Item> {
    private Node first, last;

    private class Node{
        private Item item;
        private Node next;

        public Node(Item item, Node next) {
            this.item = item;
            this.next = next;
        }

        public Node() {
        }

        public Item getItem() {
            return item;
        }

        public Node getNext() {
            return next;
        }
    }

    public boolean isEmpty() {
        return first==null;
    }

    public void enqueue(Item item) {
        Node oldLast = last;
        last = new Node();
        last.item = item;
        if (isEmpty()) {
            first = last;
        } else {
            oldLast.next = last;
        }
    }

    public Item dequeue(){
        Item item = first.item;
        first=first.next;
        if (isEmpty()) {
            last=null;
        }
        return item;
    }
}
