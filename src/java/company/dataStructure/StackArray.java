package company.dataStructure;

public class StackArray<Item> {
    private Item[] stack;
    private int N =0;

    public StackArray() {
        stack=(Item[]) new Object[1];
    }

    public void push(Item item){
        if(N==stack.length){
            resize(N*2);
        }
        stack[N]=item;
        N++;
    }

    public Item pop() {
        Item item=stack[--N];
        stack[N]=null;
        if (N > 0 && N < stack.length / 4) {
            resize(stack.length/2);
        }
        return item;
    }

    public boolean isEmpty(){
        return stack.length==0;
    }

    public int size() {
        return N;
    }

    private void resize(int size) {
        Item[] copy = (Item[]) new Object[size];
        for (int i = 0; i < stack.length; i++) {
            copy[i]=stack[i];
        }
        stack=copy;
    }
}
